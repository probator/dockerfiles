#!/usr/bin/env sh

TAG="${CI_COMMIT_TAG:-dev}"
IMAGE="probator/pybuild:${TAG}"

docker build -f Dockerfile.python -t py-temp .
CONTAINER=$(docker run -d py-temp)
docker export $CONTAINER | docker import -m 'Python Build container for Probator' - $IMAGE

# Only push non-dev versions
if [ "${TAG}" != "dev" ]; then
    docker tag $IMAGE probator/pybuild:latest
    docker push $IMAGE
    docker push probator/pybuild:latest
fi
