#!/usr/bin/env sh

TAG="${CI_COMMIT_TAG:-dev}"
IMAGE="probator/jsbuild:${TAG}"

docker build -f Dockerfile.frontend -t js-temp .
CONTAINER=$(docker run -d js-temp)
docker export $CONTAINER | docker import -m 'Frontend (node) Build container for Probator' - $IMAGE

# Only push non-dev versions
if [ "${TAG}" != "dev" ]; then
    docker tag $IMAGE probator/jsbuild:latest
    docker push $IMAGE
    docker push probator/jsbuild:latest
fi
